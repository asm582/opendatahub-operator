
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="using-aicoe-jupyterhub"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Using AICoE JupyterHub
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

AICoE-JupyterHub is based on the link:https://github.com/jupyter-on-openshift/jupyterhub-quickstart[JupyterHub on OpenShift] project and designed to allow each Jupyter user to customize their Jupyter instance by selecting a preferred notebook image, notebook resource limits and add any custom environment variables.  It uses the same link:https://github.com/jupyter-on-openshift/jupyter-notebooks[Jupyter notebooks] that are compatible with JupyterHub on Openshift.

.Prerequisites

None

.Procedure

. Navigate to the route that is created for the JupyterHub server

. Login with your OpenShift cluster username and password.

. Click the 'Authorize selected permissions' button to allow the jupyterhub-hub service account to have read-only access to your OpenShift `user:info`

. Customize the `Spawner Options` for your Jupyter notebook requirements
** `JupyterHub Server Image` - Will allow you to select any Jupyter notebook image with '-notebook' in the name as the base image for your Jupyter notebook instance
** `Deployment sizes` - Select the resource size that you require for your Jupyter notebook instance
** `GPU` - Select the number of GPUs you would like to attach to your Jupyter notebook instance. This will only work if you have enabled GPU access for the JupyterHub server
** `Environment Variables` - Custom environment varaibles you can add to your Jupyter notebook instance. By default, `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` will be provided and you can provide the values as neccessary. You can provide an additional environment variable by entering the environment variable name (`Variable name`) and value (`Variable value`)

. Click `Spawn` to spawn your Jupyter notebook instance

Once the Jupyter instance is spawned, you can create or upload any Jupyter notebooks necessary for your workflow. You can review many of the link:../tutorials/README.md[tutorial] workshop notebooks to test many of the components available in the Open Data Hub

//.Verification steps
//(Optional) Provide the user with verification method(s) for the procedure, such as expected output or commands that can be used to check for success or failure.

.Additional resources

* link:https://github.com/jupyter-on-openshift/jupyterhub-quickstart[OpenShift compatible version of the JupyterHub application]
* link:https://github.com/vpavlin/jupyter-notebooks[OpenShift compatible S2I builder for basic notebook images]
* link:https://github.com/aicoe/jupyterhub-ocp-oauth[JupyterHub deployment using OpenShift OAuth authenticator]
* link:https://github.com/vpavlin/jupyterhub-singleuser-profiles[Library to manage and configure singleuser JupyterHub servers deployed by KubeSpawner]
